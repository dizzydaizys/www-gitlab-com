title: GitLab and HashiCorp
canonical_path: /partners/technology-partners/hashicorp/
seo_title: GitLab and HashiCorp 
description: Unlock work-faster workflows to build better applications with GitLab and HashiCorp, a joint solution for secure GitOps automation.
title_description: Unlock work-faster workflows to build better applications with GitLab and HashiCorp, a joint solution for secure GitOps automation.
logo: /images/logos/hashicorp.svg
logo_alt: HashiCorp Logo
body_top_title: Uniting developers and operators with secure workflows
body_top_description: GitLab is the DevOps platform delivered as a single application for everyone on your pipeline. Integrate HashiCorp Vault and Terraform with GitLab to standardize secrets management and secure GitOps workflows.
body_top_cta_1_text: Learn more
body_top_cta_2_text: Start your free trial
body_top_cta_img_1: "/images/topics/featured-partner-hashicorp-1.png"
body_top_cta_img_2: "/images/topics/featured-partner-hashicorp-2.png"
body_top_cta_1_url: /blog/2019/09/17/gitlab-hashicorp-terraform-vault-pt-1/
body_top_cta_2_url: /free-trial/
body_top_cta_1_ga_name: Hashicorp - Learn more
body_top_cta_2_ga_name: free trial
body_top_cta_1_ga_location: hero
body_top_cta_2_ga_location: hero
body_features_title: Streamline infrastructure and application delivery with GitLab and HashiCorp 
body_features_description: GitLab shrinks cycle times from hours to minutes, helping enterprise customers embrace the cloud via automated workflows. GitLab’s built-in planning, monitoring, and reporting solutions integrate with Terraform and Vault so cross-functional teams can quickly codify infrastructure and define service dependencies within a secure environment. 
body_quote: Now it's so easy to deploy something and roll it back if there's an issue. It's taken the stress and the fear out of deploying into production.
body_quote_source: Dave Bullock, Director of Engineering at Wag!
feature_items:
  - title: "Iterate"
    description: "See progressive contributions. Version control and collaboration reduce rework so happier developers can expand product roadmaps instead of repairing old roads."
  - title: "Automate"
    description: "Secure the left way. Automated DevSecOps workflows increase uptime by reducing security and compliance risks for cloud operations."
  - title: "Innovate"
    description: "Create, impress, repeat. Increase market share and revenue when your product is on budget, on time, and always up."
body_middle_title: "Get started with GitLab and HashiCorp Joint Solutions"
body_middle_items:
  - title: "Terraform Cloud + GitLab.com"
    description: "Configure GitLab as a Git provider and version control system (VCS) for Terraform Cloud to store plans and sentinel policies to trigger automation pipelines in the cloud."
    icon: "/images/icons/icon-6.svg"
  - title: "GitLab Provider"
    description: "Use Terraform to manage resources on your GitLab instance like groups, projects, users, and more to improve productivity by eliminating an engineer’s dependence on provisioning requests."
    icon: "/images/icons/icon-5.svg"
  - title: "Terraform EE + GitLab EE"
    description: "Provide flexible, template-driven modular workflows via GitLab CI/CD that evoke Terraform plans for Infrastructure as Code (IaC)."
    icon: "/images/icons/icon-4.svg"
  - title: "Vault"
    description: "Vault is a single security control plane for operations and infrastructure. Many organizations choose Vault to manage Audit Command Language (ACL), secrets, and other sensitive data. As a joint solution, GitLab and Vault provide a cross-functional alternative to error-prone, document-based collaboration methods. Vault is the leading solution for secrets management and one of GitLab’s most popular customer workflow integration requests for DevSecOps."
    icon: "/images/icons/icon-3.svg"
benefits_title: Discover the benefits of GitLab and HashiCorp
featured_video_title: "HashiCorp Vault and GitLab integration: Why and How?"
featured_video_url: "https://www.youtube.com/watch?v=VmQZwfgp3aA"
featured_video_topic: "GitLab and HashiCorp"
resources:
  - title: "GitOps:The Future of Infrastructure Automation - A panel discussion with Weaveworks, HashiCorp, Red Hat, and GitLab"
    url: /why/gitops-infrastructure-automation/
    type: Whitepapers
    ga_name: GitOps infrastructure automation
    ga_location: body
  - title: "GitLab and HashiCorp: Providing application and infrastructure delivery workflows"
    url: /blog/2019/09/17/gitlab-hashicorp-terraform-vault-pt-1/
    type: Blog
    ga_name: Gitlab and HashiCorp - Terraform
    ga_location: body
  - title: How Wag! cut their release process from 40 minutes to just 6
    url: /blog/2019/01/16/wag-labs-blog-post/
    type: Blog
    ga_name: Wag Labs post
    ga_location: body
  - title: GitLab and HashiCorp - A holistic guide to GitOps and the Cloud Operating Model
    url: /webcast/gitlab-hashicorp-gitops/
    type: Webcast
    ga_name: Gitlab and HashiCorp - GitOps
    ga_location: body
  - title: Empowering Developers and Operators through GitLab and HashiCorp
    url: https://www.hashicorp.com/resources/empowering-developers-and-operators-through-gitlab-and-hashicorp/
    type: Whitepapers
    ga_name: GitLab and HashiCorp - developers and operators
    ga_location: body
cta_banner:
  - title: How can your team collaborate, operationalize, and secure with GitLab and HashiCorp? 
    button_url: /sales/
    button_text: Talk to an expert
    ga_name: sales
    ga_location: body
