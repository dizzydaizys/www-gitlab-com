---
layout: handbook-page-toc
title: "Employment Law at GitLab"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
Employment law refers to the law that governs the relationship between employer and employee, so the Employment section of GitLab Legal interacts with the People Group on a regular basis to provide information and legal advice related to the entire arc of our team members’ relationship with GitLab.  **So what does that mean?**


- It means we work closely with the Tax and People Operations team to determine scalable employment solutions for the Company as it hires across the globe.
- It means we support the talent acquisition department in meeting its goals and ensuring compliance with all local laws and regulations as they source, recruit, and hire new team members.
- It means we help the People Operations team onboard new team members and get them the information they need that applies both to their location and to their job responsibilities.
- It means we support the Total Rewards group in developing policies that meet GitLab’s needs, our team members’ needs, and local requirements.
- It means we support our Team Member Relations team, People Business Partners, and Total Rewards as they manage team member relations, requests for [reasonable accommodation](/handbook/people-policies/inc-usa/#reasonable-accommodation) or adjustments, performance, promotions, transfers, and any other types of issues that pop up during the course of the relationship.
- It means we support our Diversity, Inclusion, and Belonging team in ensuring that decisions across the entire arc of the relationship match our mission statement.
- And it means we provide support when it comes to end of employment decisions,  offboarding, and any follow-up matters that can occur.

GitLab’s employment attorney provides information regarding legal risk and compliance requirements based on particular jurisdictions and particular job types. Often, it is up to the others within the organization whether or not to continue down a certain path once the risks and requirements are known.

## Working with Legal on Employment Issues
When individual team members have questions about their employment, they should interact with the People Group rather than reaching out directly to the Legal team. Visit the People Group Handbook to determine who to work with for your particular needs.

If People Group needs to engage with Legal on a particular question, they will reach out to Legal staff via DMs in Slack or via email in order to ensure that details of the situation and team member personal information remain confidential. This respects employee privacy and retains attorney-client privilege.

Below are some common employment topics where Legal is asked to provide input. The paragraphs below are intended to assist People Group to know when to reach out to Legal.

**Note: GitLab team members should *always* obtain approval from the Director of Legal, Employment prior to speaking with outside counsel regarding employment-related matters.**

### Before Hire:
**Talent Acquisition.** Legal reviews sourcing and talent acquisition strategy for compliance and advises about requirements for applicant tracking in the various countries in which we hire.

**Background Check.** Legal reviews overall background check policy and procedure in place, helps develop background check adjudication process, and serves as escalation point when PBP responsible for reviewing background check has questions or concerns about specific results.

### During Hire
**Contract review.** Legal should be asked to review negotiated contracts that seek to change standard terms.

**Conversions.** As GitLab expands our international presence through new entities, some current contractors may become GitLab employees. Legal assists with this process.

**Compliance with onboarding issues.** As different countries have different requirements for hiring and disclosures, Legal will assist People Group to ensure that onboarding issues are compliant for specific locales.

### During Employment
**Total Rewards.** People Group is responsible for determining what benefits should be provided. Legal is available for advice when determining whether those benefits comply with local law, how benefits will be communicated, acknowledged via signature, and tracked.

**Relocation request reviews.** [Relocation](/handbook/people-group/relocation/) requests are handled by People Group, which has a very thorough explanation of the process and requirements in the People Group Handbook. After People Group team members have worked through their workflow, Legal is available to assist with specific unresolved questions.

**Conflicts of Interest.** If a GitLab team member engages People Group about a possible conflict of interest, the PBP should encourage discussion between the team member and their manager, as many conflict of interest determinations are business decisions, rather than legal decisions. If the team member and manager agree that there is no conflict then the applicable E-Group member can provide written approval, which is maintained in Bamboo HR.  Legal is involved in managing the process to ensure that decisions regarding what is or is not a conflict remain consistent across departments and so that the manager and team member can understand the potential risks and implications in order to make an informed decision.

### General Business Needs involving Employment Issues
**Scalable employment solutions.** As GitLab continues to grow, our systems must be scalable. GitLab Legal can assist in developing solutions that ease the work burden on People Group while maintaining compliance with legal requirements.

**Policy development.** Legal should be asked to perform a final review when policies are updated or created.

**Contract negotiations.** In the event that a customer or vendor has questions regarding terms that affect our team members (such as drug tests or more intensive background checks), the Contract team will reach out to the Employment team.

### ABCs of Employment Law

This is a non-exhaustive list of a few employment compliance matters that you should be aware of and keep in mind when communicating with candidates and team members as well as making any sort of employment-related decisions. This is _not_ meant to serve as a complete guide, but as a reminder that there are an abundance of employment compliance matters that we must adhere to and to highlight the importance of careful communication. 

<details>
  <summary markdown="span">Age</summary>
  Pretty universally across our jurisdictions we cannot make employment decisions (hiring, performance management, termination) because of a person’s age.
</details>

<details>
  <summary markdown="span">Background Checks</summary>
We run background checks on all candidates who have reached the background check phase of the interview process; however, the type of information we can request, or what we can do with the information we receive, depends on the rules of the country.
</details>

<details>
  <summary markdown="span">Contracts</summary>
For most people, the terms and conditions of employment depends on what’s in the employment contract.
</details>

<details>
  <summary markdown="span">Discrimination</summary>
Universally we do not discriminate based on protected characteristics such as age, race, color, religion, national origin, gender, or disability.
</details>

<details>
  <summary markdown="span">Employer of Record</summary>
GitLab has team members across the globe.  In some locations where we do not directly employ team members through a GitLab entity, we use Professional Employer Organizations such as Remote.com and Global Upside to serve as our Employer of Record.
</details>

<details>
  <summary markdown="span">Family</summary>
Generally, all jurisdictions have various leave laws that protect time you need to spend with family members, such as parental leave or sick leave to care for qualifying family members. Our [handbook](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#entity-benefits) includes information on statutory entitlements for team members in our entity locations.
</details>

<details>
  <summary markdown="span">GDPR (General Data Protection Regulation)</summary>
Privacy laws also limit the amount of information we can collect on our team members, including sensitive personal information.
</details>

<details>
  <summary markdown="span">Harassment</summary>
Please review our [Anti-Harassment Policy](https://about.gitlab.com/handbook/anti-harassment/) to ensure that no team member should ever feel as if they are being harassed.
</details>

<details>
  <summary markdown="span">Interactive Process</summary>
This is the process that we must engage in to determine if someone needs an accommodation for a medical or religious reason. Please note it’s a two way street. Transparency is important to ensure that we come up with the best possible solution.
</details>

<details>
  <summary markdown="span">"I was just joking!"</summary>
This is never a defense to a claim of any sort of harassment.  
</details>

<details>
  <summary markdown="span">Keeping Personnel Information Confidential</summary>
We keep this information in BambooHR and only those with certain access rights based on their particular job functions have access to the information.
</details>

<details>
  <summary markdown="span">Leave</summary>
Please check our [handbook](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#introduction) for all the different types of paid and unpaid leave available based on your jurisdiction.  This requires transparency and letting our leave administrator know the reason for leaving so that we can track it appropriately.  As a company we have tracking obligations, and you may have entitlements you didn’t know you had available to you.
</details>

<details>
  <summary markdown="span">Military</summary>
There are certain leave protections for various forms of military service.  Check the handbook.
</details>

<details>
  <summary markdown="span">National Origin</summary>
This is another protected characteristic.  We don’t make decisions based on someone’s national origin.
</details>

<details>
  <summary markdown="span">On Call</summary>
Work rules vary for team members across our organization based on their location.  Some jurisdictions have limitations on the overall number of hours worked, some have limitations on the particular days worked, etc.
</details>

<details>
  <summary markdown="span">PTO</summary>
At GitLab, we have a Don’t Ask, Do Tell policy, but we ask that you do be transparent and intentional about the time that you take and that you monitor your team members to ensure (1) that they’re taking enough time, (2) that coverage still exists when they take the time, and (3) that if they are taking what seems to be excessive time, you find out why.  It could be that they have a reason that they actually need to take leave.
</details>

<details>
  <summary markdown="span">Quid Pro Quo</summary>
This is a special kind of harassment as defined by US law.  It basically means “you scratch my back and I’ll scratch yours.” This is not an arrangement that you should ever present to another team member, nor should it ever be presented to you.
</details>

<details>
  <summary markdown="span">Retaliation</summary>
We don’t retaliate against anyone who has engaged in protected activity.
</details>

<details>
  <summary markdown="span">Sexual Harassment</summary>
Harassment takes all forms based on any number of protected characteristics (age, gender, race, national origin, religion, etc.).  Sexual harassment is a particular form based on harassment due to someone’s gender.
</details>

<details>
  <summary markdown="span">Team Member Relations</summary>
Our team that investigates concerns about team member performance, conduct, etc.  It’s also the team that reviews accommodation requests. TMR works in conjunction with Legal and the PBP’s when employment decisions must be made.
</details>

<details>
  <summary markdown="span">Unpaid Leave</summary>
There may be times someone needs to go on unpaid leave.  This will be handled by our Absence Management team.
</details>

<details>
  <summary markdown="span">Vaccine</summary>
We have to watch all the rulings around the globe to come up with as simple and safe a policy and process as possible with regard to vaccine mandates.
</details>

<details>
  <summary markdown="span">Whistleblower</summary>
Our [EthicsPoint hotline](http://gitlab.ethicspoint.com/) is available should anyone have a complaint that they wish to raise anonymously.
</details>

<details>
  <summary markdown="span">Generation X, Y, and Z </summary>
Generation X, Y, and Z, and even some Boomers, are all in the workforce together.  We all give and take feedback differently, work differently, and have different expectations.  Managers have to be aware of these generational differences and manage accordingly. 
</details>



