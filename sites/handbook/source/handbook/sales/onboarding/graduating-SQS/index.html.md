---
layout: handbook-page-toc
title: "Graduating from Sales Onboarding"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Graduating from Sales Onboarding
In order to officially “graduate” from Sales Onboarding at GitLab, we have identified a few components that are considered milestones of achievement (many of these must be done concurrently to ensure completion):

Sales, Customer Success and SDR Roles:
*  Complete Sales Quick Start learning path in EdCast prior to the SQS Workshop (at least 80% completion required, though 100% is preferred)
*  Complete the Sales Quick Start (SQS) Workshop
*  Articulate the Command of the Message Mantra
*  Articulate the 30 second GitLab elevator pitch (memorized)
*  Complete 2 discovery calls:
   - Submit 1 recorded mock discovery call in SQS pre-work
   - Complete 1 mock discovery call at the SQS Workshop with a PMM, Manager, or a more experienced SA or TAM

For now, the same requirements apply for all customer-facing roles. As we transition all the coursework into EdCast, our new Learning Experience Platform (LXP), we may create more role-specific requirements for graduation. Upon completion of SQS, the final grade report will be provided to the managers for review.
